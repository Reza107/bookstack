@if(setting('app-logo', '') !== 'none')
    <?php
    if (setting('app-logo', '') === '') {  
        if (config('app.rtl')) {?>
            <div class="header-logo-desktop">
            <svg width="175" height="40" viewBox="0 0 175 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g clip-path="url(#clip0_20_184)">
                    <path d="M 152.759 4 C 155.726 4 158.626 4.88 161.093 6.528 C 163.559 8.177 165.482 10.519 166.617 13.26 C 167.753 16.001 168.05 19.017 167.471 21.927 C 166.892 24.836 165.463 27.509 163.366 29.607 C 161.268 31.705 158.595 33.133 155.685 33.712 C 152.776 34.291 149.76 33.994 147.019 32.859 C 144.278 31.723 141.935 29.801 140.287 27.334 C 138.639 24.867 137.759 21.967 137.759 19 C 137.762 15.023 139.344 11.21 142.156 8.397 C 144.968 5.585 148.782 4.004 152.759 4 Z" fill="url(#paint0_linear_20_184)"/>
                    <path d="M 147.759 12 L 153.759 26 L 151.759 26 L 145.759 12 L 147.759 12 Z" fill="#FFC0C4"/>
                    <path d="M 153.759 12 L 159.759 26 L 157.759 26 L 151.759 12 L 153.759 12 Z" fill="#FFC0C4"/>
                    <path d="M 159.759 12 L 157.759 12 L 157.759 26 L 159.759 26 L 159.759 12 Z" fill="white"/>
                    <path d="M 153.759 12 L 151.759 12 L 151.759 26 L 153.759 26 L 153.759 12 Z" fill="white"/>
                    <path d="M 147.759 12 L 145.759 12 L 145.759 26 L 147.759 26 L 147.759 12 Z" fill="white"/>
                    <path d="M 88 12 L 80 12 L 80 14 L 88 14 L 88 12 Z" fill="black" class="filter-white"/>
                    <path d="M 120 12 L 112 12 L 112 14 L 120 14 L 120 12 Z" fill="black" class="filter-white"/>
                    <path d="M 54 14 L 56 14 L 56 18 L 52 18 L 52 16 L 54 16 L 54 14 Z" fill="black" class="filter-white"/>
                    <path d="M 78 14 L 76 14 L 76 16 L 78 16 L 78 14 Z" fill="black" class="filter-white"/>
                    <path d="M 64 16 L 62 16 L 62 18 L 64 18 L 64 16 Z" fill="black" class="filter-white"/>
                    <path d="M 84 16 L 82 16 L 82 24 L 84 24 L 84 16 Z" fill="black" class="filter-white"/>
                    <path d="M 100 16 L 98 16 L 98 24 L 100 24 L 100 16 Z" fill="black" class="filter-white"/>
                    <path d="M 116 16 L 114 16 L 114 24 L 116 24 L 116 16 Z" fill="black" class="filter-white"/>
                    <path d="M 126 16 L 128 16 L 128 22 L 130 22 L 130 20 L 132 20 L 132 24 L 126 24 L 126 16 Z" fill="black" class="filter-white"/>
                    <path d="M 132 16 L 130 16 L 130 18 L 132 18 L 132 16 Z" fill="black" class="filter-white"/>
                    <path d="M 48 20 L 50 20 L 50 22 L 58 22 L 58 20 L 60 20 L 60 22 L 62 22 L 62 20 L 64 20 L 64 22 L 66 22 L 66 20 L 68 20 L 68 24 L 50 24 L 50 26 L 42 26 L 42 24 L 48 24 L 48 20 Z" fill="black" class="filter-white"/>
                    <path d="M 74 18 L 74 22 L 72 22 L 72 20 L 70 20 L 70 26 L 72 26 L 72 24 L 80 24 L 80 18 L 74 18 Z M 78 22 L 76 22 L 76 20 L 78 20 L 78 22 Z" fill="black" class="filter-white"/>
                    <path d="M 92 20 L 90 20 L 90 22 L 92 22 L 92 20 Z" fill="black" class="filter-white"/>
                    <path d="M 94 20 L 96 20 L 96 26 L 88 26 L 88 24 L 94 24 L 94 20 Z" fill="black" class="filter-white"/>
                    <path d="M 104 20 L 102 20 L 102 26 L 104 26 L 104 20 Z" fill="black" class="filter-white"/>
                    <path d="M 106 18 L 106 24 L 110 24 L 110 26 L 112 26 L 112 18 L 106 18 Z M 110 22 L 108 22 L 108 20 L 110 20 L 110 22 Z" fill="black" class="filter-white"/>
                    <path d="M 118 18 L 118 26 L 120 26 L 120 24 L 124 24 L 124 18 L 118 18 Z M 122 22 L 120 22 L 120 20 L 122 20 L 122 22 Z" fill="black" class="filter-white"/>
                    <path d="M 68 26 L 64 26 L 64 28 L 68 28 L 68 26 Z" fill="black" class="filter-white"/>
                </g>
            <defs>
                <linearGradient id="paint0_linear_20_184" x1="152.59" y1="32.75" x2="168.52" y2="7.49" gradientUnits="userSpaceOnUse" gradientTransform="matrix(1, 0, 0, 1, -7.241486, -0.999997)">
                <stop stop-color="#FF1E4B"/>
                <stop offset="1" stop-color="#FF4141"/>
                </linearGradient>
                <clipPath id="clip0_20_184">
                <rect width="175" height="40" fill="black"/>
                </clipPath>
            </defs>
            </svg>
            </div>
            <div class="header-logo-mobile">
                <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <g clip-path="url(#clip0_427_60)">
                    <path d="M32 16C32 7.16344 24.8366 0 16 0C7.16344 0 0 7.16344 0 16C0 24.8366 7.16344 32 16 32C24.8366 32 32 24.8366 32 16Z" fill="#FF3644"/>
                    <path d="M21.1732 22.0806L16.0762 9.92004" stroke="#F2A6AC" stroke-width="2"/>
                    <path d="M15.9202 22.0806L10.8232 9.92004" stroke="#F2A6AC" stroke-width="2"/>
                    <path d="M16 22.4478V16V9.55225" stroke="white" stroke-width="2"/>
                    <path d="M10.7461 22.4478V9.55225" stroke="white" stroke-width="2"/>
                    <path d="M21.2539 22.4478V16V9.55225" stroke="white" stroke-width="2"/>
                    </g>
                    <defs>
                    <clipPath id="clip0_427_60">
                    <rect width="32" height="32" fill="white"/>
                    </clipPath>
                    </defs>
                </svg>
            </div>    
            
        <?php
        }
        else{
            ?>
            <svg width="200" height="60" viewBox="0 0 200 60" fill="none" xmlns="http://www.w3.org/2000/svg">
                <g clip-path="url(#clip0_440_258)">
                    <mask id="mask0_440_258" style="mask-type:luminance" maskUnits="userSpaceOnUse" x="0" y="0" width="200" height="60">
                        <path d="M200 0H0V60H200V0Z" fill="white"/>
                    </mask>
                    <g mask="url(#mask0_440_258)">
                        <mask id="mask1_440_258" style="mask-type:luminance" maskUnits="userSpaceOnUse" x="-63" y="0" width="263" height="60">
                            <path d="M199.5 0H-63V60H199.5V0Z" fill="white"/>
                        </mask>
                        <g mask="url(#mask1_440_258)">
                            <path d="M22.5 7.5C26.951 7.5 31.3 8.81959 35.001 11.2919C38.7 13.7643 41.585 17.2783 43.287 21.3897C44.991 25.5009 45.437 30.0249 44.568 34.3896C43.7 38.7541 41.556 42.7632 38.41 45.9099C35.263 49.0566 31.254 51.1995 26.889 52.0677C22.525 52.9359 18.001 52.4903 13.89 50.7873C9.778 49.0844 6.26401 46.2004 3.79201 42.5004C1.32001 38.8002 0 34.4501 0 30C0.005 24.0341 2.37799 18.3139 6.59599 14.0954C10.814 9.87682 16.535 7.50477 22.5 7.5Z" fill="url(#paint0_linear_440_258)"/>
                            <path d="M15 19.5L24 40.5H21L12 19.5H15Z" fill="#FFC0C4"/>
                            <path d="M24 19.5L33 40.5H30L21 19.5H24Z" fill="#FFC0C4"/>
                            <path d="M33 19.5H30V40.5H33V19.5Z" fill="white"/>
                            <path d="M24 19.5H21V40.5H24V19.5Z" fill="white"/>
                            <path d="M15 19.5H12V40.5H15V19.5Z" fill="white"/>
                            <path d="M134 18H122V21H134V18Z" fill="black" class="filter-white"/>
                            <path d="M182 18H170V21H182V18Z" fill="black" class="filter-white"/>
                            <path d="M83 21H86V27H80V24H83V21Z" fill="black" class="filter-white"/>
                            <path d="M119 21H116V24H119V21Z" fill="black" class="filter-white"/>
                            <path d="M98 24H95V27H98V24Z" fill="black" class="filter-white"/>
                            <path d="M128 24H125V36H128V24Z" fill="black" class="filter-white"/>
                            <path d="M152 24H149V36H152V24Z" fill="black" class="filter-white"/>
                            <path d="M176 24H173V36H176V24Z" fill="black" class="filter-white"/>
                            <path d="M191 24H194V33H197V30H200V36H191V24Z" fill="black" class="filter-white"/>
                            <path d="M200 24H197V27H200V24Z" fill="black" class="filter-white"/>
                            <path d="M74 30H77V33H89V30H92V33H95V30H98V33H101V30H104V36H77V39H65V36H74V30Z" fill="black" class="filter-white"/>
                            <path d="M113 27V33H110V30H107V39H110V36H122V27H113ZM119 33H116V30H119V33Z" fill="black" class="filter-white"/>
                            <path d="M140 30H137V33H140V30Z" fill="black" class="filter-white"/>
                            <path d="M143 30H146V39H134V36H143V30Z" fill="black" class="filter-white"/>
                            <path d="M158 30H155V39H158V30Z" fill="black" class="filter-white"/>
                            <path d="M161 27V36H167V39H170V27H161ZM167 33H164V30H167V33Z" fill="black" class="filter-white"/>
                            <path d="M179 27V39H182V36H188V27H179ZM185 33H182V30H185V33Z" fill="black" class="filter-white"/>
                            <path d="M104 39H98V42H104V39Z" fill="black" class="filter-white"/>
                        </g>
                    </g>
                </g>
                <defs>
                    <linearGradient id="paint0_linear_440_258" x1="11.385" y1="49.125" x2="35.28" y2="11.235" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#FF1E4B"/>
                        <stop offset="1" stop-color="#FF4141"/>
                    </linearGradient>
                    <clipPath id="clip0_440_258">
                        <rect width="200" height="60" fill="white"/>
                    </clipPath>
                </defs>
            </svg>

        <?php
        }
    } elseif (setting('app-logo', '') !== '') {
        ?>
        <img class="logo-image" src="{{ url(setting('app-logo', '')) }}" alt="Logo">
        <?php
    }
    ?>
@endif