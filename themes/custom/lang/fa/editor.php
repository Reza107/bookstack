<?php
/**
 * Page Editor Lines
 * Contains text strings used within the user interface of the
 * WYSIWYG page editor. Some Markdown editor strings may still
 * exist in the 'entities' file instead since this was added later.
 */
return [
    // Toolbar
    'edit_code_block' => 'ویرایش بلوک کد',
    // Images, links, details/summary & embed
    'open_link' => 'باز کردن لینک',
    'open_link_in' => 'باز کردن لینک در...',
    'remove_link' => 'حذف لینک',
];
