<?php
/**
 * Authentication Language Lines
 * The following language lines are used during authentication for various
 * messages that we need to display to the user.
 */
return [
    // Login auto-initiation
    'auto_init_starting_desc' => 'برای شروع فرآیند ورود در حال ارتباط با سیستم احراز هویت شما تماس هستیم. اگر پس از 5 ثانیه پیشرفتی حاصل نشد، می توانید روی لینک زیر کلیک کنید.',
    'auto_init_start_link' => 'احراز هویت را ادامه دهید',
    // Email Confirmation
    'email_confirm_thanks' => 'برای تأییدتان سپاسگزاریم!',
    'email_confirm_thanks_desc' => 'لطفاً یک لحظه صبر کنید تا تأیید شما بررسی شود. اگر بعد از 3 ثانیه هدایت نشدید، لینک "ادامه" زیر را فشار دهید تا ادامه دهید.',
];
