<?php

/**
 * Text used for user-preference specific views within bookstack.
 */

return [
    'shortcuts' => 'کلید های میانبر',
    'shortcuts_interface' => 'میانبرهای صفحه کلید رابط',
    'shortcuts_toggle_desc' => 'در اینجا می توانید کلید های میانبر صفحه کلید را که برای پیمایش و اقدامات استفاده می شود، فعال یا غیرفعال کنید.',
    'shortcuts_customize_desc' => 'می توانید هر یک از میانبرهای زیر را سفارشی سازی کنید. کافی است پس از انتخاب ورودی برای میانبر، کلید ترکیبی مورد نظر خود را فشار دهید.',
    'shortcuts_toggle_label' => 'میانبرهای صفحه کلید فعال شد',
    'shortcuts_section_navigation' => 'پیمایش',
    'shortcuts_section_actions' => 'اقدامات متداول',
    'shortcuts_save' => 'ذخیره میانبرها',
    'shortcuts_overlay_desc' => 'توجه: هنگامی که میانبرها فعال هستند، یک پوشش کمکی با فشار دادن "?" در دسترس است. که میانبرهای موجود برای اقداماتی که در حال حاضر روی صفحه قابل مشاهده است را برجسته می کند.',
    'shortcuts_update_success' => 'تنظیمات میانبرها بروزرسانی شد!',
];