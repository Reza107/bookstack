<?php
/**
 * Text shown in error messaging.
 */
return [
    // Drawing & Images
    'drawing_data_not_found' => 'داده های طراحی بارگیری نشد. ممکن است فایل طراحی، دیگر وجود نداشته باشد یا شما اجازه دسترسی به آن را نداشته باشید.',
];
